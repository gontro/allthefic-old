from django.db.models.signals import m2m_changed, pre_delete, pre_save

from .models import Fandom, Story, ChapterRating


class FandomSignals:
    @staticmethod
    def pre_delete(sender, instance, using, **kwargs):
        for section in instance.sections.all():
            section.set_stat('fandoms', 1, '-')

    @staticmethod
    def sections_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for section in instance.sections.all():
                section.set_stat('fandoms', 1, '-')
        if action == 'post_add':
            for section in instance.sections.all():
                section.set_stat('fandoms', 1, '+')


class StorySignals:
    @staticmethod
    def pre_delete(sender, instance, using, **kwargs):
        for fandom in instance.fandom.all():
            fandom.set_stat('fandoms', 1, '-')

    @staticmethod
    def fandoms_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for fandom in instance.fandoms.all():
                fandom.set_stat('stories', 1, '-')
        if action == 'post_add':
            for fandom in instance.fandoms.all():
                fandom.set_stat('stories', 1, '+')


class ChapterRatingSignals:
    @staticmethod
    def pre_save(sender, instance, raw, using, update_fields, **kwargs):
        try:
            prev_instance = ChapterRating.objects.get(pk=instance.pk)
        except ChapterRating.DoesNotExist:
            prev_instance = None
        story = instance.story
        chapter = instance.get_chapter()
        if prev_instance:
            if prev_instance.rating != instance.rating:
                if instance.rating:
                    story.set_stat('likes', 1, '+')
                    chapter.set_stat('likes', 1, '+')
                    story.set_stat('dislikes', 1, '-')
                    chapter.set_stat('dislikes', 1, '-')
                else:
                    story.set_stat('likes', 1, '-')
                    chapter.set_stat('likes', 1, '-')
                    story.set_stat('dislikes', 1, '+')
                    chapter.set_stat('dislikes', 1, '+')
        else:
            if instance.rating:
                story.set_stat('likes', 1, '+')
                chapter.set_stat('likes', 1, '+')
            else:
                story.set_stat('dislikes', 1, '+')
                chapter.set_stat('dislikes', 1, '+')



pre_delete.connect(FandomSignals.pre_delete, sender=Fandom)
m2m_changed.connect(FandomSignals.sections_changed, sender=Fandom.sections.through)

pre_delete.connect(StorySignals.pre_delete, sender=Story)
m2m_changed.connect(StorySignals.fandoms_changed, sender=Story.fandoms.through)

pre_save.connect(ChapterRatingSignals.pre_save, sender=ChapterRating)