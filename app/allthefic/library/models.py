from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

from ..core.utils import word_count
from ..core.mixins import StatsCacheMixin


STORY_STATUSES = [(False, 'In-Progress'), (True, 'Complete')]


class Language(models.Model):
    name = models.CharField(max_length=30, unique=True)
    slug = models.SlugField(max_length=30, unique=True)

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Language, self).save(*args, **kwargs)

    class Meta:
        ordering = ['name']


class Genre(models.Model):
    name = models.CharField(max_length=20, unique=True)
    slug = models.SlugField(max_length=20, editable=False, unique=True)

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Genre, self).save(*args, **kwargs)

    class Meta:
        ordering = ['name']


class Rating(models.Model):
    code = models.CharField(max_length=1, unique=True)
    name = models.CharField(max_length=25)
    age_group = models.PositiveSmallIntegerField()
    is_restricted = models.BooleanField()
    description = models.TextField()

    def __unicode__(self):
        return "%s - %s" % (self.code, self.name)

    class Meta:
        ordering = ['id']


class Section(StatsCacheMixin, models.Model):
    name = models.CharField(max_length=10, unique=True)
    slug = models.SlugField(max_length=10, editable=False, unique=True)

    default_stats = {'fandoms': 0, 'stories': 0, 'communities': 0}

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Section, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('library:fandom_list', kwargs={'section_slug': self.slug})

    def get_community_url(self):
        return reverse('community:fandom_list', kwargs={'section_slug': self.slug})

    class Meta:
        ordering = ['name']


class Fandom(StatsCacheMixin, models.Model):
    sections = models.ManyToManyField(Section)
    name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True, editable=False)

    default_stats = {'stories': 0, 'communities': 0}

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Fandom, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('library:story_list', kwargs={'fandom_slug': self.slug})

    class Meta:
        ordering = ['name']


class Character(models.Model):
    fandom = models.ForeignKey(Fandom)
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, editable=False)

    def __unicode__(self):
        return unicode(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Character, self).save(*args, **kwargs)

    class Meta:
        ordering = ['name']
        unique_together = (("fandom", "name"), ("fandom", "slug"))


class Story(StatsCacheMixin, models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='stories')
    title = models.CharField(max_length=100, help_text="The title of the story.")
    slug = models.SlugField(max_length=100, editable=False)
    summary = models.TextField(max_length=400)
    disclaimer = models.TextField(max_length=1000)

    fandoms = models.ManyToManyField(Fandom)
    characters = models.ManyToManyField(Character, blank=True, null=True)

    language = models.ForeignKey(Language)
    rating = models.ForeignKey(Rating)
    genres = models.ManyToManyField(Genre)  # Maximum of 3 genres allowed

    total_chapters = models.PositiveIntegerField(default=0)
    total_words = models.PositiveIntegerField(default=0)

    contains_slash = models.BooleanField(default=False)
    allow_anonymous_reviews = models.BooleanField(default=True)
    is_complete = models.BooleanField(default=False)

    update_date = models.DateField(auto_now=True)
    publish_date = models.DateField(auto_now_add=True)

    default_stats = {'communities': 0, 'reviews': 0, 'followers': 0, 'favourites': 0, 'likes': 0, 'dislikes': 0}

    @property
    def chapters(self):
        return Chapter.objects.filter(story=self).order_by('number', '-revision').distinct('number')

    def __unicode__(self):
        return unicode(self.title)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.author.total_stories += 1
        self.slug = slugify(self.title)
        super(Story, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('library:chapter_detail', kwargs={'story_id': self.id, 'chapter_number': 1, 'story_slug': self.slug})

    def get_last_chapter_url(self):
        return reverse('library:chapter_detail', kwargs={'story_id': self.id, 'chapter_number': self.total_chapters, 'story_slug': self.slug})

    def get_whole_story_url(self):
        return reverse('library:story_detail', kwargs={'story_id': self.id, 'story_slug': self.slug})

    def get_followers_list_url(self):
        return reverse('library:followers_list', kwargs={'story_id': self.id, 'story_slug': self.slug})

    def get_favourites_list_url(self):
        return reverse('library:favourites_list', kwargs={'story_id': self.id, 'story_slug': self.slug})

    def get_reviewers_list_url(self):
        return reverse('library:reviewers_list', kwargs={'story_id': self.id, 'story_slug': self.slug})

    def get_follow_url(self):
        return reverse('library:follow_story', kwargs={'story_id': self.id})

    def get_favourite_url(self):
        return reverse('library:favourite_story', kwargs={'story_id': self.id})

    class Meta:
        ordering = ['-update_date']
        verbose_name_plural = 'stories'


class Chapter(StatsCacheMixin, models.Model):
    story = models.ForeignKey(Story, related_name="chapters")
    number = models.PositiveSmallIntegerField()
    revision = models.PositiveSmallIntegerField(default=1)

    title = models.CharField(max_length=40, blank=True)
    content = models.TextField()
    start_notes = models.TextField(blank=True)
    end_notes = models.TextField(blank=True)

    total_words = models.PositiveIntegerField(default=0)

    publish_date = models.DateField(auto_now_add=True)

    default_stats = {'reviews': 0, 'likes': 0, 'dislikes': 0}

    def __unicode__(self):
        if self.title:
            return u'Chapter %d: %s' % (self.number, self.title)
        return u'Chapter %d' % self.number

    def save(self, *args, **kwargs):
        if not self.pk:
            self.story.total_chapters += 1
            self.number = self.story.total_chapters
        else:
            self.pk = None
            self.publish_date = None
            self.revision = Chapter.objects.filter(story=self.story, number=self.number).count() + 1
        self.story.total_words -= self.total_words
        self.total_words = word_count(self.content)
        super(Chapter, self).save(*args, **kwargs)
        self.story.total_words += self.total_words
        self.story.save()

    def get_cache_key(self):
        return "Story[%d]::Chapter-%d::Stats" % (self.story.id, self.number)

    def get_absolute_url(self):
        return reverse('library:chapter_detail', kwargs={'story_id': self.story_id, 'chapter_number': self.number, 'story_slug': self.story.slug})

    def get_next_chapter_url(self):
        if self.number != self.story.total_chapters:
            return reverse('library:chapter_detail', kwargs={'story_id': self.story_id, 'chapter_number': self.number + 1, 'story_slug': self.story.slug})
        return ''

    def get_prev_chapter_url(self):
        if self.number != 1:
            return reverse('library:chapter_detail', kwargs={'story_id': self.story_id, 'chapter_number': self.number - 1, 'story_slug': self.story.slug})
        return ''

    def get_rating_url(self):
        return reverse('library:rate_chapter', kwargs={'story_id': self.story_id, 'chapter_number': self.number})

    class Meta:
        ordering = ['story', 'number', '-revision']
        unique_together = (('story', 'number', 'revision'),)


class ChapterRating(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    story = models.ForeignKey(Story)
    chapter_number = models.PositiveSmallIntegerField()
    rating = models.BooleanField()

    created_date = models.DateTimeField(auto_now_add=True)

    def get_chapter(self):
        return Chapter.objects.filter(story=self.story, number=self.chapter_number).latest('revision')

    def __unicode__(self):
        return unicode(self.rating)

    class Meta:
        ordering = ['id']
        unique_together = (('user', 'story', 'chapter_number'),)




# ChapterComment -> annotate selected parts of story
# ChapterCommentRating -> rate comments for usefulness ala stackoverflow