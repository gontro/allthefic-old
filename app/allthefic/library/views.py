from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView, View

from braces.views import JSONResponseMixin

from ..core.messages import FOLLOW_STORY, UNFOLLOW_STORY, FAVOURITE_STORY, UNFAVOURITE_STORY, LIKE_CHAPTER, DISLIKE_CHAPTER
from ..core.mixins import FilterAndOrderMixin

from .models import Section, Fandom, Story, Chapter, ChapterRating
from .forms import StoryListFilterForm


class FandomListView(ListView):
    model = Fandom

    def get_context_data(self, **kwargs):
        context = super(FandomListView, self).get_context_data(**kwargs)
        context['section'] = get_object_or_404(Section, slug=self.kwargs['section_slug'])
        return context

    def get_queryset(self):
        queryset = super(FandomListView, self).get_queryset()
        queryset = queryset.prefetch_related('sections').filter(sections__slug=self.kwargs['section_slug'])
        return queryset


class StoryListView(FilterAndOrderMixin, ListView):
    model = Story
    filters = {
        'characters': 'characters__id',
        'genres': 'genres__id__in',
        'language': 'language__id',
        'rating': 'rating__id',
        'words': 'total_words__gte',
        'status': 'is_complete'
    }
    ordering = {
        'sort_by': {
            'update': ('-update_date',),
            'publish': ('-publish_date', '-update_date'),
            'words': ('-total_words', '-update_date'),
            # @todo: figure out how to use redis stat data in ordering
            # 'reviews': ('-stats__reviews', '-update_date'),
            # 'favourites': ('-stats__favourites', '-update_date'),
            # 'follows': ('-stats__follows', '-update_date'),
        }
    }

    def get_context_data(self, **kwargs):
        context = super(StoryListView, self).get_context_data(**kwargs)
        context['fandom'] = get_object_or_404(Fandom, slug=self.kwargs['fandom_slug'])
        context['filter_form'] = StoryListFilterForm(self.request.GET)
        return context

    def get_queryset(self):
        queryset = super(StoryListView, self).get_queryset()
        queryset = queryset.select_related('author', 'rating', 'language').prefetch_related('genres', 'fandoms', 'characters').filter(fandoms__slug=self.kwargs['fandom_slug'])
        if not queryset.ordered:
            queryset = queryset.order_by('-update_date')
        return queryset


class StoryView(DetailView):
    model = Story

    def get_context_data(self, **kwargs):
        context = super(StoryView, self).get_context_data(**kwargs)
        context['viewing_whole_story'] = True
        return context

    def get_queryset(self):
        return self.model.objects.select_related('author', 'rating', 'language').prefetch_related('chapters', 'genres', 'fandoms', 'characters').filter(id=self.kwargs['story_id'])

    def get_object(self, queryset=None):
        return self.get_queryset()[0]


class ChapterView(DetailView):
    model = Chapter

    def get_context_data(self, **kwargs):
        context = super(ChapterView, self).get_context_data(**kwargs)
        context['story'] = Story.objects.select_related('author', 'rating', 'language').prefetch_related('chapters', 'genres', 'fandoms', 'characters').filter(id=self.kwargs['story_id'])[0]
        try:
            if self.request.user.is_authenticated():
                chapter_rating = ChapterRating.objects.get(user=self.request.user, story=context['story'], chapter_number=self.kwargs['chapter_number'])
            else:
                chapter_rating = None
        except ChapterRating.DoesNotExist:
            chapter_rating = None
        context['chapter_rating'] = chapter_rating
        return context

    def get_queryset(self):
        return self.model.objects.filter(story_id=self.kwargs['story_id'], number=self.kwargs['chapter_number'])

    def get_object(self, queryset=None):
        return self.get_queryset()[0]


class FollowStoryView(JSONResponseMixin, View):
    def get(self, request, story_id):
        stop = request.GET.get('stop')
        try:
            status = 'success'
            story = Story.objects.get(pk=story_id)
            if stop:
                request.user.following_stories.remove(story)
            else:
                request.user.following_stories.add(story)
        except Story.DoesNotExist:
            status = 'error'
        message = UNFOLLOW_STORY[status] if stop else FOLLOW_STORY[status]
        return self.render_json_response({'status': status, 'message': message})


class FavouriteStoryView(JSONResponseMixin, View):
    def get(self, request, story_id):
        stop = request.GET.get('stop')
        try:
            status = 'success'
            story = Story.objects.get(pk=story_id)
            if stop:
                request.user.favourite_stories.remove(story)
            else:
                request.user.favourite_stories.add(story)
        except Story.DoesNotExist:
            status = 'error'
        message = UNFAVOURITE_STORY[status] if stop else FAVOURITE_STORY[status]
        return self.render_json_response({'status': status, 'message': message})


class RateChapterView(JSONResponseMixin, View):
    def get(self, request, story_id, chapter_number):
        dislike = request.GET.get('dislike')
        try:
            status = 'success'
            story = Story.objects.get(pk=story_id)
            try:
                chapter_rating = ChapterRating.objects.get(user=request.user, story=story, chapter_number=chapter_number)
            except ChapterRating.DoesNotExist:
                chapter_rating = ChapterRating(user=request.user, story=story, chapter_number=chapter_number)
            if dislike:
                chapter_rating.rating = False
            else:
                chapter_rating.rating = True
            chapter_rating.save()
        except Story.DoesNotExist:
            status = 'error'
        message = DISLIKE_CHAPTER[status] if dislike else LIKE_CHAPTER[status]
        return self.render_json_response({'status': status, 'message': message})
