from django import forms

from django_select2 import ModelSelect2MultipleField

from ..library.models import Rating, Language, Genre, Character


class StoryListFilterForm(forms.Form):
    SORTING_CHOICES = (('update', 'Update Date'), ('publish', 'Publish Date'), ('words', 'Word Count'), ('reviews', 'Reviews'), ('favourites', 'Favourites'), ('follows', 'Follows'))
    WORD_COUNT_CHOICES = (('', '---------'), (1000, '> 1000'), (5000, '> 5000'), (10000, '> 10,000'), (20000, '> 20,000'), (40000, '> 40,000'), (60000, '> 60,000'), (100000, '> 100,000'), (150000, '> 150,000'), (250000, '> 250,000'), (500000, '> 500,000'))
    STATUS_CHOICES = (('', '---------'), (0, 'In-progress'), (1, 'Complete'))

    status = forms.ChoiceField(choices=STATUS_CHOICES)
    rating = forms.ModelChoiceField(queryset=Rating.objects)
    language = forms.ModelChoiceField(queryset=Language.objects)
    words = forms.ChoiceField(choices=WORD_COUNT_CHOICES)
    genres = ModelSelect2MultipleField(queryset=Genre.objects)
    characters = ModelSelect2MultipleField(queryset=Character.objects)
    sort_by = forms.ChoiceField(choices=SORTING_CHOICES)