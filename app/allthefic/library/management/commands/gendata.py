import random

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

from ...models import Fandom, Story, Chapter, Language, Rating, Genre, Character


class Command(BaseCommand):
    help = 'Generates Lorem Ipsum test data'

    def handle(self, *args, **options):
        text = open('allthefic/library/management/commands/_ipsum.txt', 'r').read()
        paragraphs = text.split('\n')
        paragraph = paragraphs[0].split(' ')
        language = Language.objects.get(slug='english')
        fandoms = Fandom.objects.all()
        genres = Genre.objects.all()
        for author_index in range(0, 5):
            author = get_user_model().objects.create_user(
                username=slugify(paragraph[random.randint(1, len(paragraph))]),
                email=slugify(paragraph[random.randint(1, len(paragraph))]) + '@gmail.com',
                bio=paragraphs[random.randrange(1, len(paragraphs))]
            )
        for story_index in range(0, 50):
            print '\n1. Story'
            authors = get_user_model().objects.all()
            if not authors:
                print "FAILED - no authors.."
                break
            try:
                author = authors.order_by('?')[0]
                start = random.randrange(0, int(len(paragraph) / 2))
                end = random.randrange(start + 1, start + 10)
                title = ' '.join(paragraph[start:end])
                is_complete = False
                if random.randint(1, 5) == 1:
                    is_complete = True
                story = Story.objects.create(
                    author=author,
                    title=title,
                    summary=paragraphs[random.randrange(1, len(paragraphs))],
                    disclaimer=paragraphs[random.randrange(1, len(paragraphs))],
                    language=language,
                    rating=Rating.objects.order_by('?')[0],
                    is_complete=is_complete
                )
            except:
                continue

            print '2. Fandoms'
            fandoms_added = []
            if fandoms.count() < 3:
                count = random.randint(1, fandoms.count())
            else:
                count = random.randint(1, 3)
            for fandom_index in range(0, count):
                fandom = fandoms.order_by('?')[0]
                if fandom not in fandoms_added:
                    fandoms_added.append(fandom)
                    story.fandoms.add(fandom)

            print '3. Genres'
            genres_added = []
            count = random.randint(1, 3)
            for genre_index in range(0, count):
                genre = genres.order_by('?')[0]
                if genre not in genres_added:
                    genres_added.append(genre)
                    story.genres.add(genre)

            print '4. Characters'
            characters = Character.objects.filter(fandom=fandoms_added[0])
            characters_added = []
            count = random.randint(1, 5)
            for character_index in range(0, count):
                character = characters.order_by('?')[0]
                if character not in characters_added:
                    characters_added.append(character)
                    story.characters.add(character)

            print '5. Chapters'
            for chapter_index in range(1, random.randrange(2, 31)):
                paragraph = paragraphs[0].split(' ')
                start = random.randrange(0, int(len(paragraph) / 2))
                end = random.randrange(start + 1, start + 5)
                title = ' '.join(paragraph[start:end])
                content = ''
                for paragraph_index in range(0, random.randrange(1, 151)):
                    content += '<p>%s</p>' % paragraphs[paragraph_index]
                chapter = Chapter.objects.create(
                    story=story,
                    title=title,
                    content=content,
                    start_notes=paragraphs[random.randrange(1, len(paragraphs))],
                    end_notes=paragraphs[random.randrange(1, len(paragraphs))]
                )