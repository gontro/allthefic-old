from django.core.management.base import BaseCommand

from ...models import Section, Fandom, Story, ChapterRating
from ....community.models import Review, Community, CommunityLike
from ....account.models import User, AuthorLike


class Command(BaseCommand):
    help = 'Reload redis stat data'

    def handle(self, *args, **options):
        # Section Stats
        for section in Section.objects.all():
            section_stats = section.default_stats
            section_stats['fandoms'] = Fandom.objects.prefetch_related('sections').filter(sections=section).count()
            section_stats['stories'] = Story.objects.prefetch_related('fandoms', 'fandoms__sections').filter(fandoms__sections=section).count()
            section_stats['communities'] = Community.objects.prefetch_related('fandoms', 'fandoms__sections').filter(fandoms__sections=section).count()
            section.set_stats(section_stats)

        # Fandom Stats
        for fandom in Fandom.objects.all():
            fandom_stats = fandom.default_stats
            fandom_stats['stories'] = Story.objects.prefetch_related('fandoms').filter(fandoms=fandom).count()
            fandom_stats['communities'] = Community.objects.prefetch_related('fandoms').filter(fandoms=fandom).count()
            fandom.set_stats(fandom_stats)

        # User Stats
        for user in User.objects.all():
            user_stats = user.default_stats
            users = User.objects.prefetch_related('following_authors', 'favourite_authors')
            user_stats['followers'] = users.filter(following_authors=user).count()
            user_stats['favourites'] = users.filter(favourite_authors=user).count()
            user_stats['likes'] = AuthorLike.objects.filter(author=user).count()
            user.set_stats(user_stats)

        # Community Stats
        for community in Community.objects.all():
            community_stats = community.default_stats
            users = User.objects.prefetch_related('following_communities', 'favourite_communities')
            community_stats['followers'] = users.filter(following_communities=community).count()
            community_stats['favourites'] = users.filter(favourite_communities=community).count()
            community_stats['likes'] = CommunityLike.objects.filter(community=community).count()
            community.set_stats(community_stats)

        # Story and Chapter Stats
        for story in Story.objects.all():
            story_stats = story.default_stats
            users = User.objects.prefetch_related('following_stories', 'favourite_stories')
            story_stats['communities'] = Community.objects.prefetch_related('fandoms').filter(stories=story).count()
            story_stats['followers'] = users.filter(following_stories=story).count()
            story_stats['favourites'] = users.filter(favourite_stories=story).count()
            for chapter in story.chapters.all():
                chapter_stats = chapter.default_stats
                chapter_ratings = ChapterRating.objects.filter(story=story, chapter_number=chapter.number)
                chapter_stats['reviews'] = Review.objects.filter(chapter=chapter).count()
                chapter_stats['likes'] = chapter_ratings.filter(rating=True).count()
                chapter_stats['dislikes'] = chapter_ratings.filter(rating=False).count()
                chapter.set_stats(chapter_stats)
                story_stats['reviews'] += chapter_stats['reviews']
                story_stats['likes'] += chapter_stats['likes']
                story_stats['dislikes'] += chapter_stats['dislikes']
            story.set_stats(story_stats)

