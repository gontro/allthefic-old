from django.conf.urls import patterns, url

from .views import FandomListView, StoryListView, StoryView, FollowStoryView, FavouriteStoryView, RateChapterView, ChapterView


urlpatterns = patterns('',
    url(r'^section/(?P<section_slug>[\w-]+)$', FandomListView.as_view(), name='fandom_list'),
    url(r'^browse/(?P<fandom_slug>[\w-]+)$', StoryListView.as_view(), name='story_list'),

    url(r'^story/(?P<story_id>\d+)/follow$', FollowStoryView.as_view(), name='follow_story'),
    url(r'^story/(?P<story_id>\d+)/favourite$', FavouriteStoryView.as_view(), name='favourite_story'),
    url(r'^story/(?P<story_id>\d+)/(?P<chapter_number>\d+)/rate$', RateChapterView.as_view(), name='rate_chapter'),

    url(r'^story/(?P<story_id>\d+)/(?P<story_slug>[\w-]+)?$', StoryView.as_view(), name='story_detail'),
    url(r'^story/(?P<story_id>\d+)/(?P<chapter_number>\d+)/(?P<story_slug>[\w-]+)?$', ChapterView.as_view(), name='chapter_detail'),
)