from ..library.models import Section


SECTIONS = None


def sections(request):
    global SECTIONS

    if SECTIONS is None:
        SECTIONS = Section.objects.all()

    return {'sections': SECTIONS}