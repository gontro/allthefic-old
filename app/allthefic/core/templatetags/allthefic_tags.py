from django.template import Library

from ..utils import calculate_percentage


register = Library()

register.filter('percentage', calculate_percentage)