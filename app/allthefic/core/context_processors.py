from utils import split_path


def nav(request):
    selected = {'app': None, 'action': None, 'mod': None}
    formats = ['json', 'csv', 'pdf']

    path_tokens = split_path(request.path)
    for index, token in enumerate(path_tokens):
        bits = token.split('.')
        if len(bits) >= 2 and bits[-1] in formats:
            path_tokens[index] = '.'.join(bits[:-1])

    apps = ['browse', 'communities', 'account', 'admin']
    if len(path_tokens) and path_tokens[0] in apps:
        selected['app'] = path_tokens[0]
        if len(path_tokens) >= 2:
            selected['action'] = path_tokens[1]
        if len(path_tokens) >= 3:
            selected['mod'] = path_tokens[2]

    return {'nav': selected}