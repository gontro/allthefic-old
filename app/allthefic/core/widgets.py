import json

from django.forms import widgets
from django.utils.safestring import mark_safe


class DatePickerWidget(widgets.TextInput):
    def render(self, name, value, attrs=None):
        js_attrs = ""
        if self.attrs.has_key("js_attrs"):
            js_attrs = json.dumps(self.attrs["js_attrs"])
            del self.attrs["js_attrs"]

        widget_html = super(DatePickerWidget, self).render(name, value, attrs)
        widget_html += mark_safe("""
        <script type="text/javascript">$(function(){$("#id_%s").datepicker(%s)});</script>
        """ % (name, js_attrs))

        return widget_html

    class Media:
        css = {
            'all': ('datepicker/css/datepicker.css',)
        }
        js = (
            'datepicker/js/bootstrap-datepicker.js',
        )