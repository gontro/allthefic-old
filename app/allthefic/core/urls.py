from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView

from .views import HomepageView, TextPlainView


urlpatterns = patterns('',
    # Base URLs
    url(r'^$', HomepageView.as_view(), name='homepage'),

    # Admin URLs
    url(r'', include('allthefic.admin.urls', 'admin')),

    # Library URLs
    url(r'', include('allthefic.library.urls', 'library')),

    # Community URLs
    url(r'', include('allthefic.community.urls', 'community')),

    # Account URLs
    url(r'', include('allthefic.account.urls', 'account')),

    # Social Auth URLs
    url(r'', include('social_auth.urls')),

    # Other
    url(r'^favicon\.ico$', RedirectView.as_view(url=settings.MEDIA_URL + 'images/favicon.ico')),
    url(r'^crossdomain\.xml$', TextPlainView.as_view(template_name='crossdomain.xml')),
    url(r'^humans\.txt$', TextPlainView.as_view(template_name='humans.txt')),
    url(r'^robots\.txt$', TextPlainView.as_view(template_name='robots.txt')),
)


if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
    )