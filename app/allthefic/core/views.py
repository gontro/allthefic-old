from django.views.generic import TemplateView


class HomepageView(TemplateView):
    template_name = "homepage.html"


class TextPlainView(TemplateView):
    def render_to_response(self, context, **kwargs):
        return super(TextPlainView, self).render_to_response(context, content_type='text/plain', **kwargs)