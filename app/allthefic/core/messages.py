FOLLOW_AUTHOR = {
    'success': 'Successfully following author.',
    'error': 'An error has occurred, could not follow author!'
}

UNFOLLOW_AUTHOR = {
    'success': 'Successfully stopped following author.',
    'error': 'An error has occurred, could not stop following author!'
}

FAVOURITE_AUTHOR = {
    'success': 'Successfully added author to favourites.',
    'error': 'An error has occurred, could not add author to favourites!'
}

UNFAVOURITE_AUTHOR = {
    'success': 'Successfully removed author from favourites.',
    'error': 'An error has occurred, could not remove author from favourites!'
}

FOLLOW_STORY = {
    'success': 'Successfully following story.',
    'error': 'An error has occurred, could not follow story!'
}

UNFOLLOW_STORY = {
    'success': 'Successfully stopped following story.',
    'error': 'An error has occurred, could not stop following story!'
}

FAVOURITE_STORY = {
    'success': 'Successfully added story to favourites.',
    'error': 'An error has occurred, could not add story to favourites!'
}

UNFAVOURITE_STORY = {
    'success': 'Successfully removed story from favourites.',
    'error': 'An error has occurred, could not remove story from favourites!'
}

LIKE_CHAPTER = {
    'success': 'You have liked this chapter.',
    'error': 'An error has occurred, could not like this chapter.'
}

DISLIKE_CHAPTER = {
    'success': 'You have disliked this chapter.',
    'error': 'An error has occurred, could not dislike this chapter.'
}