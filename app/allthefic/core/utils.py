import re

from django.utils.html import strip_tags


def split_path(path):
    return [token for token in path.split('/') if token]


def word_count(content, format=0, charlist=''):
    if isinstance(content, str) or isinstance(content, unicode):
        content = strip_tags(content)
        words = re.sub('[^\w ' + charlist + ']', '', content)
        while '  ' in words:
            words = words.replace('  ', ' ')
        words = words.split(' ')
        if format == 0:
            return len(words)
        elif format == 1:
            return words
        elif format == 2:
            result = {}
            for word in words:
                result[content.find(word)] = word
            return result
    return False


def calculate_percentage(amount1, amount2):
    try:
        total = float(amount1) + float(amount2)
        if total == 0:
            return '0%'
        return "%.2f%%" % (float(amount1) / total * 100)
    except ValueError:
        return ''