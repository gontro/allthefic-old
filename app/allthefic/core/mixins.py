from django.core.cache import cache


class StatsCacheMixin(object):
    @property
    def stats(self):
        return self.get_stats()

    def get_cache_key(self):
        return '%s[%s]::Stats' % (self.__class__.__name__, str(self.pk))

    def get_stats(self):
        key = self.get_cache_key()
        stats = cache.get(key)
        if stats is None:
            stats = self.default_stats
            self.set_stats(stats)
        return stats

    def set_stats(self, stats):
        cache.set(self.get_cache_key(), stats, timeout=0)
        return stats

    def set_stat(self, key, value, operation='='):
        if key not in self.default_stats.keys():
            return False
        if operation not in ['=', '+', '-']:
            return False
        stats = self.get_stats()
        if operation == '=':
            stats[key] = value
        elif operation == '+':
            stats[key] += value
        elif operation == '-':
            stats[key] -= value
        return self.set_stats(stats)


class OrderMixin(object):
    ordering = {}

    def order_queryset(self, queryset):
        ordering = []
        for item in self.ordering:
            if item in self.request.GET.keys():
                key = self.request.GET[item]
                order_by_params = self.ordering[item]
                if not isinstance(order_by_params, dict):
                    continue
                for order_by_key, order_by in order_by_params.items():
                    if not isinstance(order_by, tuple) and not isinstance(order_by, list):
                        continue
                    if order_by_key == key:
                        ordering.extend(order_by)
                        break
        return queryset.order_by(*ordering)

    def get_queryset(self):
        queryset = super(OrderMixin, self).get_queryset()
        if self.ordering:
            queryset = self.order_queryset(queryset)
        return queryset


class FilterMixin(object):
    filters = {}

    def filter_queryset(self, queryset):
        filters = {}
        for item in self.filters:
            if item in self.request.GET.keys():
                filter = self.filters[item]
                if filter.endswith("__in"):
                    filters[filter] = self.request.GET.getlist(item)
                else:
                    keys = self.request.GET.getlist(item)
                    if len(keys) == 1:
                        key = self.request.GET[item]
                        if key != '':
                            filters[filter] = key
                    else:
                        for key in keys:
                            queryset = queryset.filter(**{filter: key})
        return queryset.filter(**filters)

    def get_queryset(self):
        queryset = super(FilterMixin, self).get_queryset()
        if self.filters:
            queryset = self.filter_queryset(queryset)
        return queryset


class FilterAndOrderMixin(FilterMixin, OrderMixin):
    def get_queryset(self):
        queryset = super(FilterAndOrderMixin, self).get_queryset()
        if self.filters:
            queryset = self.filter_queryset(queryset)
        if self.ordering:
            queryset = self.order_queryset(queryset)
        return queryset