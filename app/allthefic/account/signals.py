from django.db.models.signals import m2m_changed, pre_delete

from .models import User


class UserSignals:
    @staticmethod
    def pre_delete(sender, instance, using, **kwargs):
        for story in instance.following_stories.all():
            story.set_stat('followers', 1, '-')
        for author in instance.following_authors.all():
            author.set_stat('followers', 1, '-')
        for community in instance.following_communities.all():
            community.set_stat('followers', 1, '-')
        for story in instance.favourite_stories.all():
            story.set_stat('favourites', 1, '-')
        for author in instance.favourite_authors.all():
            author.set_stat('favourites', 1, '-')
        for community in instance.favourite_communities.all():
            community.set_stat('favourites', 1, '-')

    @staticmethod
    def following_stories_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for story in instance.following_stories.all():
                story.set_stat('followers', 1, '-')
        if action == 'pre_remove':
            for story in instance.following_stories.filter(pk__in=pk_set):
                story.set_stat('followers', 1, '-')
        if action == 'post_add':
            for story in instance.following_stories.filter(pk__in=pk_set):
                story.set_stat('followers', 1, '+')

    @staticmethod
    def following_authors_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for author in instance.following_authors.all():
                author.set_stat('followers', 1, '-')
        if action == 'pre_remove':
            for author in instance.following_authors.filter(pk__in=pk_set):
                author.set_stat('followers', 1, '-')
        if action == 'post_add':
            for author in instance.following_authors.filter(pk__in=pk_set):
                author.set_stat('followers', 1, '+')

    @staticmethod
    def following_communities_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for community in instance.following_communities.all():
                community.set_stat('followers', 1, '-')
        if action == 'pre_remove':
            for community in instance.following_communities.filter(pk__in=pk_set):
                community.set_stat('followers', 1, '-')
        if action == 'post_add':
            for community in instance.following_communities.filter(pk__in=pk_set):
                community.set_stat('followers', 1, '+')

    @staticmethod
    def favourite_stories_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for story in instance.favourite_stories.all():
                story.set_stat('favourites', 1, '-')
        if action == 'pre_remove':
            for story in instance.favourite_stories.filter(pk__in=pk_set):
                story.set_stat('favourites', 1, '-')
        if action == 'post_add':
            for story in instance.favourite_stories.filter(pk__in=pk_set):
                story.set_stat('favourites', 1, '+')

    @staticmethod
    def favourite_authors_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for author in instance.favourite_authors.all():
                author.set_stat('favourites', 1, '-')
        if action == 'pre_remove':
            for author in instance.favourite_authors.filter(pk__in=pk_set):
                author.set_stat('favourites', 1, '-')
        if action == 'post_add':
            for author in instance.favourite_authors.filter(pk__in=pk_set):
                author.set_stat('favourites', 1, '+')

    @staticmethod
    def favourite_communities_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for community in instance.favourite_communities.all():
                community.set_stat('favourites', 1, '-')
        if action == 'pre_remove':
            for community in instance.favourite_communities.filter(pk__in=pk_set):
                community.set_stat('favourites', 1, '-')
        if action == 'post_add':
            for community in instance.favourite_communities.filter(pk__in=pk_set):
                community.set_stat('favourites', 1, '+')


pre_delete.connect(UserSignals.pre_delete, sender=User)
m2m_changed.connect(UserSignals.following_stories_changed, sender=User.following_stories.through)
m2m_changed.connect(UserSignals.following_authors_changed, sender=User.following_authors.through)
m2m_changed.connect(UserSignals.following_communities_changed, sender=User.following_communities.through)
m2m_changed.connect(UserSignals.favourite_stories_changed, sender=User.favourite_stories.through)
m2m_changed.connect(UserSignals.favourite_authors_changed, sender=User.favourite_authors.through)
m2m_changed.connect(UserSignals.favourite_communities_changed, sender=User.favourite_communities.through)