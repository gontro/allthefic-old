from django.conf.urls import patterns, url

from .views import AuthorView, FollowAuthorView, FavouriteAuthorView, SettingsView, ProfileView, NewStoryView, EditStoryView, ManageStoriesView


urlpatterns = patterns('',
    url(r'^~(?P<username>[\w.+-]+)$', AuthorView.as_view(), name='author_detail'),
    url(r'^author/(?P<author_id>\d+)/follow$', FollowAuthorView.as_view(), name='follow_author'),
    url(r'^author/(?P<author_id>\d+)/favourite$', FavouriteAuthorView.as_view(), name='favourite_author'),

    url(r'^account$', ProfileView.as_view(), name='dashboard'),

    url(r'^account/inbox', ProfileView.as_view(), name='inbox'),
    url(r'^account/outbox$', ProfileView.as_view(), name='outbox'),

    url(r'^account/settings$', SettingsView.as_view(), name='settings'),
    url(r'^account/profile$', ProfileView.as_view(), name='profile'),
    url(r'^account/reviews$', ProfileView.as_view(), name='reviews'),
    url(r'^account/favourites$', ProfileView.as_view(), name='favourites'),
    url(r'^account/following$', ProfileView.as_view(), name='following'),

    url(r'^account/new-story', NewStoryView.as_view(), name='new-story'),
    url(r'^account/edit-story/(?P<pk>[0-9]+)', EditStoryView.as_view(), name='edit-story'),
    url(r'^account/manage-stories$', ManageStoriesView.as_view(), name='manage-stories'),
    url(r'^account/image-manager$', ProfileView.as_view(), name='image-manager'),

    url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^logout/(?P<next_page>.*)$', 'django.contrib.auth.views.logout', name='logout_next'),
)