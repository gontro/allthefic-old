import floppyforms as forms

from django.contrib.auth import get_user_model

from ..core.widgets import DatePickerWidget
from ..library.models import Story, Language


class SettingsForm(forms.ModelForm):
    date_of_birth = forms.DateField(widget=DatePickerWidget())

    class Meta:
        model = get_user_model()
        fields = ('email', 'date_of_birth', 'allow_restricted_content')


class ProfileForm(forms.ModelForm):
    profile = forms.CharField()

    class Meta:
        model = get_user_model()
        fields = ('profile',)


class StoryForm(forms.ModelForm):
    language = forms.ModelChoiceField(Language.objects.all(), initial=Language.objects.get(slug='english'))

    class Meta:
        model = Story
        exclude = ('author',)