from django.http import Http404
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views.generic import UpdateView, DetailView, CreateView, ListView, View
from django.contrib.auth import get_user_model

from braces.views import PrefetchRelatedMixin, JSONResponseMixin

from ..core.messages import FOLLOW_AUTHOR, UNFOLLOW_AUTHOR, FAVOURITE_AUTHOR, UNFAVOURITE_AUTHOR
from ..library.models import Story

from .forms import SettingsForm, ProfileForm, StoryForm


class AuthorView(PrefetchRelatedMixin, DetailView):
    template_name = "account/author_detail.html"
    context_object_name = 'author'
    model = get_user_model()
    slug_url_kwarg = 'username'
    slug_field = 'username'
    prefetch_related = ('stories', 'stories__rating', 'stories__language', 'stories__genres', 'stories__fandoms',
                        'stories__characters', 'favourite_stories', 'favourite_stories__author',
                        'favourite_stories__rating', 'favourite_stories__language', 'favourite_stories__genres',
                        'favourite_stories__fandoms', 'favourite_stories__characters', 'favourite_authors')


class SettingsView(UpdateView):
    template_name = "account/settings.html"
    form_class = SettingsForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            messages.add_message(request, messages.SUCCESS, "Account settings updated!")
            return self.form_valid(form)
        else:
            messages.add_message(request, messages.ERROR, "Please correct the errors below!")
            return self.form_invalid(form)

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return self.request.path


class ProfileView(UpdateView):
    template_name = "account/profile.html"
    form_class = ProfileForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            messages.add_message(request, messages.SUCCESS, "Public profile updated!")
            return self.form_valid(form)
        else:
            messages.add_message(request, messages.ERROR, "Failed to update profile!")
            return self.form_invalid(form)

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return self.request.path


class NewStoryView(CreateView):
    template_name = "account/story.html"
    model = Story
    form_class = StoryForm

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            self.object = form.save(commit=False)
            self.object.author = request.user
            self.object.save()
            messages.add_message(request, messages.SUCCESS, "New story created!")
            return self.form_valid(form)
        else:
            messages.add_message(request, messages.ERROR, "Failed to create story!")
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse('account:edit-story', kwargs={'pk': self.object.id})


class EditStoryView(UpdateView):
    template_name = "account/story.html"
    model = Story
    form_class = StoryForm

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.author != request.user and not request.user.is_staff and not request.user.is_superuser:
            raise Http404("Permission denied.")
        return super(EditStoryView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.author != request.user and not request.user.is_staff and not request.user.is_superuser:
            raise Http404("Permission denied.")
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            messages.add_message(request, messages.SUCCESS, "Story updated!")
            return self.form_valid(form)
        else:
            messages.add_message(request, messages.ERROR, "Failed to update story!")
            return self.form_invalid(form)

    def get_success_url(self):
        return self.request.path


class ManageStoriesView(ListView):
    template_name = "account/manage-stories.html"
    model = Story

    def get_queryset(self):
        queryset = super(ManageStoriesView, self).get_queryset()
        queryset.filter(author=self.request.user)
        return queryset


class FollowAuthorView(JSONResponseMixin, View):
    def get(self, request, author_id):
        stop = request.GET.get('stop')
        try:
            status = 'success'
            author = get_user_model().objects.get(pk=author_id)
            if stop:
                request.user.following_authors.remove(author)
            else:
                request.user.following_authors.add(author)
        except get_user_model().DoesNotExist:
            status = 'error'
        message = UNFOLLOW_AUTHOR[status] if stop else FOLLOW_AUTHOR[status]
        return self.render_json_response({'status': status, 'message': message})


class FavouriteAuthorView(JSONResponseMixin, View):
    def get(self, request, author_id):
        stop = request.GET.get('stop')
        try:
            status = 'success'
            author = get_user_model().objects.get(pk=author_id)
            if stop:
                request.user.favourite_authors.remove(author)
            else:
                request.user.favourite_authors.add(author)
        except get_user_model().DoesNotExist:
            status = 'error'
        message = UNFAVOURITE_AUTHOR[status] if stop else FAVOURITE_AUTHOR[status]
        return self.render_json_response({'status': status, 'message': message})