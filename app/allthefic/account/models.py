import re

from django.db import models
from django.core import validators
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from ..core.mixins import StatsCacheMixin


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None, **extra_fields):
        email = self.normalize_email(email)
        try:
            user = self.get(email=email)
            if password:
                user.set_password(password)
                user.save()
        except ObjectDoesNotExist:
            user = self.model(username=username, email=email, **extra_fields)
            user.set_password(password)
            # @todo: remove debug code
            if username == 'gontro':
                user.is_staff = True
                user.is_superuser = True
            user.save()
            # @todo: catch IntegrityError on username and provide a method for the user to change the username
        return user

    def create_staff(self, username, email, password, **extra_fields):
        user = self.create_user(username, email, password, **extra_fields)
        user.is_staff = True
        user.save()
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        user = self.create_user(username, email, password, **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class User(StatsCacheMixin, AbstractBaseUser):
    username = models.CharField(max_length=30, unique=True, db_index=True,
        help_text=_('Required. 30 characters or fewer. Letters, numbers and ./+/-/_ characters'),
        validators=[validators.RegexValidator(re.compile('^[\w.+-]+$'), _('Enter a valid username.'), 'invalid')]
    )
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    date_of_birth = models.DateField(blank=True, null=True)
    bio = models.TextField(blank=True)

    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    allow_restricted_content = models.BooleanField(default=False)

    following_stories = models.ManyToManyField('library.Story', related_name='following_set', blank=True, null=True)
    following_authors = models.ManyToManyField('self', related_name='following_set', blank=True, null=True)
    following_communities = models.ManyToManyField('community.Community', related_name='following_set', blank=True, null=True)

    favourite_stories = models.ManyToManyField('library.Story', related_name='favourited_set', blank=True, null=True)
    favourite_authors = models.ManyToManyField('self', related_name='favourited_set', blank=True, null=True)
    favourite_communities = models.ManyToManyField('community.Community', related_name='favourited_set', blank=True, null=True)

    total_stories = models.PositiveIntegerField(default=0)

    date_joined = models.DateTimeField(auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    default_stats = {'followers': 0, 'favourites': 0, 'likes': 0}

    def __unicode__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('account:author_detail', kwargs={'username': self.username})

    def get_follow_url(self):
        return reverse('account:follow_author', kwargs={'author_id': self.pk})

    def get_favourite_url(self):
        return reverse('account:favourite_author', kwargs={'author_id': self.pk})

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    class Meta:
        ordering = ['-date_joined']


class AuthorLike(models.Model):
    author = models.ForeignKey(User, related_name='author')
    user = models.ForeignKey(User)

    def __unicode__(self):
        return unicode(self.id)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.author.set_stat('likes', 1, '+')
        super(AuthorLike, self).save(*args, **kwargs)

    class Meta:
        ordering = ['id']
        unique_together = (('author', 'user'),)