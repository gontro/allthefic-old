from django.contrib.auth import get_user_model
from django.views.generic import ListView

from .models import Community


class FandomListView(ListView):
    model = Community


class FollowersListView(ListView):
    model = get_user_model()


class FavouritesListView(ListView):
    model = get_user_model()


class ReviewersListView(ListView):
    model = get_user_model()