from django.conf.urls import patterns, url

from .views import FandomListView, FollowersListView, FavouritesListView, ReviewersListView


urlpatterns = patterns('',
    url(r'^community/(?P<section_slug>[\w-]+)$', FandomListView.as_view(), name='fandom_list'),

    url(r'^followers/(?P<story_id>\d+)/(?P<story_slug>[\w-]+)?$', FollowersListView.as_view(), name='followers_list'),
    url(r'^favourites/(?P<story_id>\d+)/(?P<story_slug>[\w-]+)?$', FavouritesListView.as_view(), name='favourites_list'),
    url(r'^reviewers/(?P<story_id>\d+)/(?P<story_slug>[\w-]+)?$', ReviewersListView.as_view(), name='reviewers_list'),
)