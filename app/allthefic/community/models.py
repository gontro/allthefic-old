from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify

from ..core.mixins import StatsCacheMixin


class Review(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    chapter = models.ForeignKey('library.Chapter')
    content = models.TextField(max_length=500)
    reply_to = models.ForeignKey('self', blank=True, null=True)

    update_date = models.DateField(auto_now=True)
    publish_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return unicode(self.id)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.chapter.set_stat('reviews', 1, '+')
            self.chapter.story.set_stat('reviews', 1, '+')
        super(Review, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-publish_date']


class ReviewRating(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    review = models.ForeignKey(Review)
    rating = models.BooleanField()

    def __unicode__(self):
        return unicode(self.rating)

    class Meta:
        ordering = ['id']


class Community(StatsCacheMixin, models.Model):
    founder = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, editable=False, unique=True)
    summary = models.TextField(max_length=400)

    fandoms = models.ManyToManyField('library.Fandom')
    characters = models.ManyToManyField('library.Character', blank=True, null=True)
    stories = models.ManyToManyField('library.Story', blank=True, null=True)

    moderators = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='moderator_set')

    update_date = models.DateField(auto_now=True)
    created_date = models.DateField(auto_now_add=True)

    default_stats = {'followers': 0, 'favourites': 0, 'likes': 0}

    def __unicode__(self):
        return unicode(self.title)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Community, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-created_date']
        verbose_name_plural = 'communities'


class CommunityLike(models.Model):
    community = models.ForeignKey(Community)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __unicode__(self):
        return unicode(self.id)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.community.set_stat('likes', 1, '+')
        super(CommunityLike, self).save(*args, **kwargs)

    class Meta:
        ordering = ['id']
        unique_together = (('community', 'user'),)