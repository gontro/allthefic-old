from django.db.models.signals import m2m_changed, pre_delete

from .models import Community


class CommunitySignals:
    @staticmethod
    def pre_delete(sender, instance, using, **kwargs):
        for fandom in instance.fandoms.all():
            fandom.set_stat('communities', 1, '-')

    @staticmethod
    def fandoms_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for fandom in instance.fandoms.all():
                fandom.set_stat('communities', 1, '-')
        if action == 'post_add':
            for fandom in instance.fandoms.all():
                fandom.set_stat('communities', 1, '+')

    @staticmethod
    def stories_changed(sender, instance, action, reverse, model, pk_set, using, **kwargs):
        if action == 'pre_clear':
            for story in instance.stories.all():
                story.set_stat('communities', 1, '-')
        if action == 'post_add':
            for story in instance.stories.all():
                story.set_stat('communities', 1, '+')


pre_delete.connect(CommunitySignals.pre_delete, sender=Community)
m2m_changed.connect(CommunitySignals.fandoms_changed, sender=Community.fandoms.through)
m2m_changed.connect(CommunitySignals.stories_changed, sender=Community.stories.through)