from django.conf.urls import patterns, url, include

from .admin import site
from .views import DashboardView

urlpatterns = patterns('',
    url(r'^admin$', DashboardView.as_view(), name='dashboard'),
    url(r'^admin/', include(site.get_urls()))
)