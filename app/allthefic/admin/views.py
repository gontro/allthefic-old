from django.views.generic import TemplateView
from django.contrib.auth import get_user_model

from ..library.models import Section, Fandom, Language, Genre, Rating, Character

from .admin import site, AdminView


class DashboardView(TemplateView):
    template_name = "admin/dashboard.html"


class UsersView(AdminView):
    model = get_user_model()
    icon_class = "icon-user"
    datatable_columns = (
        ('id', {"sTitle": "ID"}),
        ('username', {"sTitle": "Username"}),
        ('email', {"sTitle": "Email"}),
    )


class SectionsView(AdminView):
    model = Section
    icon_class = "icon-bookmark"
    datatable_columns = (
        ('id', {"sTitle": "ID"}),
        ('name', {"sTitle": "Name"})
    )


class FandomsView(AdminView):
    model = Fandom
    icon_class = "icon-bookmark-empty"
    datatable_columns = (
        ('id', {"sTitle": "ID"}),
        ('name', {"sTitle": "Name"}),
        ('sections__name', {"sTitle": "Section"})
    )


class LanguagesView(AdminView):
    model = Language
    icon_class = "icon-comment-alt"
    datatable_columns = (
        ('id', {"sTitle": "ID"}),
        ('name', {"sTitle": "Name"})
    )


class GenresView(AdminView):
    model = Genre
    icon_class = "icon-heart-empty"
    datatable_columns = (
        ('id', {"sTitle": "ID"}),
        ('name', {"sTitle": "Name"})
    )


class RatingsView(AdminView):
    model = Rating
    icon_class = "icon-star-empty"
    datatable_columns = (
        ('id', {"sTitle": "ID"}),
        ('code', {"sTitle": "Code"}),
        ('name', {"sTitle": "Name"})
    )


class CharactersView(AdminView):
    model = Character
    icon_class = "icon-user-md"
    datatable_columns = (
        ('id', {"sTitle": "ID"}),
        ('name', {"sTitle": "Name"}),
        ('fandom__name', {"sTitle": "Fandom"})
    )


site.register(UsersView)
site.register(SectionsView)
site.register(FandomsView)
site.register(LanguagesView)
site.register(GenresView)
site.register(RatingsView)
site.register(CharactersView)