import json
import floppyforms as forms

from django.db.models import Q
from django.conf.urls import patterns, url
from django.http import HttpResponse, Http404
from django.utils.cache import add_never_cache_headers
from django.views.generic import FormView
from django.shortcuts import get_object_or_404


class AdminView(FormView):
    template_name = "admin/list.html"
    controls_column_markup = ''
    has_controls = True

    def get_params(self, request, *args, **kwargs):
        self.action = kwargs.get('action')
        self.obj_id = kwargs.get('obj_id')
        if self.obj_id and self.get_model():
            self.obj = get_object_or_404(self.get_model(), pk=self.obj_id)

    def get(self, request, *args, **kwargs):
        return self.get_response(request, False, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.get_response(request, True, *args, **kwargs)

    def get_response(self, request, is_post, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Permission denied.")
        self.get_params(request, *args, **kwargs)
        if not self.obj_id and self.action in ['view', 'edit', 'delete']:
            raise Http404("Missing obj_id.")

        response = None
        if self.action == 'list':
            response = self.action_list(is_post)
        elif self.action == 'add':
            self.template_name = "admin/form.html"
            response = self.action_add(is_post)
        elif self.action == 'view':
            self.template_name = "admin/view.html"
            response = self.action_view(is_post)
        elif self.action == 'edit':
            self.template_name = "admin/form.html"
            response = self.action_edit(is_post)
        elif self.action == 'delete':
            response = self.action_delete(is_post)

        if response is None:
            if is_post:
                response = super(AdminView, self).post(request, *args, **kwargs)
            else:
                response = super(AdminView, self).get(request, *args, **kwargs)
        return response

    def action_add(self, is_post):
        if is_post:
            SUCCESS_MSG = {'status': 'OK', 'messages': [{'code': 'success', 'title': 'Success!', 'message': 'Item successfully added!'}]}
            ERROR_MSG = {'status': 'ERROR', 'messages': [{'code': 'error', 'title': 'Error!', 'message': 'Could not add item!'}]}
            try:
                form_class = self.get_form_class()
                form = self.get_form(form_class)
                if form.is_valid():
                    form.save()
                    return self.render_to_json(SUCCESS_MSG)
                else:
                    return self.render_to_json(ERROR_MSG)
            except:
                return self.render_to_json(ERROR_MSG)
        return None

    def action_edit(self, is_post):
        if is_post:
            SUCCESS_MSG = {'status': 'OK', 'messages': [{'code': 'success', 'title': 'Success!', 'message': 'Item successfully changed!'}]}
            ERROR_MSG = {'status': 'ERROR', 'messages': [{'code': 'error', 'title': 'Error!', 'message': 'Could not change item!'}]}
            try:
                form_class = self.get_form_class()
                form = self.get_form(form_class)
                if form.is_valid():
                    form.save()
                    return self.render_to_json(SUCCESS_MSG)
                else:
                    return self.render_to_json(ERROR_MSG)
            except:
                return self.render_to_json(ERROR_MSG)
        return None

    def action_view(self, is_post):
        if is_post:
            raise Http404("Permission denied.")
        return None

    def action_list(self, is_post):
        if is_post:
            raise Http404("Permission denied.")
        return self.render_datatables()

    def action_delete(self, is_post):
        SUCCESS_MSG = {'status': 'OK', 'messages': [{'code': 'success', 'title': 'Success!', 'message': 'Item successfully deleted!'}]}
        ERROR_MSG = {'status': 'ERROR', 'messages': [{'code': 'error', 'title': 'Error!', 'message': 'Could not delete item!'}]}
        try:
            self.obj.delete()
            return self.render_to_json(SUCCESS_MSG)
        except:
            return self.render_to_json(ERROR_MSG)

    def get_form_class(self):
        form_class = super(AdminView, self).get_form_class()
        if not form_class:
            meta = type('Meta', (), {"model": self.get_model()})
            form_class = type('modelform', (forms.ModelForm,), {"Meta": meta})
        return form_class

    def get_model(self):
        if not self.model:
            raise Http404("Admin view is missing model.")
        return self.model

    def get_form_kwargs(self):
        kwargs = super(AdminView, self).get_form_kwargs()
        if hasattr(self, 'obj'):
            kwargs['instance'] = self.obj
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AdminView, self).get_context_data(**kwargs)
        context['model'] = self.get_model()._meta
        context['action'] = self.action
        aoColumns = []
        for field, params in self.get_datatable_columns():
            aoColumns.append(params)
        if self.has_controls:
            aoColumns.append({"sTitle": "&nbsp;", "bSearchable": False, "bSortable": False, "sWidth": "50px", "sClass": "center"})
        context['datatable_columns'] = json.dumps(aoColumns)
        if self.obj_id:
            context['obj_id'] = self.obj_id
        return context

    def get_queryset(self):
        return self.get_model().objects.all()

    def get_datatable_columns(self):
        if not self.datatable_columns:
            return (('id', {"sTitle": "ID"}),)
        return self.datatable_columns

    def render_datatables(self):
        queryset = self.get_queryset()
        datatable_columns = self.get_datatable_columns()

        sEcho = int(self.request.GET.get('sEcho', 0)) # Required echo response
        iColumns = int(self.request.GET.get('iColumns', 0)) # Get the number of columns
        if self.has_controls:
            iColumns -= 1
        iDisplayLength =  min(int(self.request.GET.get('iDisplayLength', 10)), 100) # Safety measure. If someone messes with iDisplayLength manually, we clip it to the max value of 100.
        iDisplayStart = int(self.request.GET.get('iDisplayStart', 0)) # Where the data starts from (page)
        iDisplayEnd = iDisplayStart + iDisplayLength  # where the data ends (end of page)

        # Pass sColumns
        aFields = [column[0] for column in datatable_columns]
        sColumns = ",".join(map(str, aFields))

        # Query optimization
        # @todo: auto select_related and prefetch_related

        # Ordering data
        iSortingCols = int(self.request.GET.get('iSortingCols', 0))
        aSortingCols = []

        if iSortingCols:
            for iColIdx in range(0, iSortingCols):
                sortedColID = int(self.request.GET.get('iSortCol_'+str(iColIdx), 0))
                if self.request.GET.get('bSortable_{0}'.format(sortedColID), 'false') == 'true': # Make sure the column is sortable first
                    sortedColName = datatable_columns[sortedColID][0]
                    sortingDirection = self.request.GET.get('sSortDir_'+str(iColIdx), 'asc')
                    if sortingDirection == 'desc':
                        sortedColName = '-'+sortedColName
                    aSortingCols.append(sortedColName)
            queryset = queryset.order_by(*aSortingCols)

        # Determine which columns are searchable
        aSearchableColumns = []
        for iColIdx in range(0, iColumns):
            if self.request.GET.get('bSearchable_{0}'.format(iColIdx), False) == 'true':
                aSearchableColumns.append(datatable_columns[iColIdx][0])

        # Apply filtering by value sent by user
        sCustomSearch = self.request.GET.get('sSearch', '').encode('utf-8')
        if sCustomSearch != '':
            outputQ = None
            for searchableColumn in aSearchableColumns:
                kwargs = {searchableColumn+"__icontains" : sCustomSearch}
                outputQ = outputQ | Q(**kwargs) if outputQ else Q(**kwargs)
            queryset = queryset.filter(outputQ)

        # Individual column search
        outputQ = None
        for iColIdx in range(0, iColumns):
            if self.request.GET.get('sSearch_{0}'.format(iColIdx), False) > '' and self.request.GET.get('bSearchable_{0}'.format(iColIdx), False) == 'true':
                kwargs = {datatable_columns[iColIdx][0]+"__icontains" : self.request.GET['sSearch_{0}'.format(iColIdx)]}
                outputQ = outputQ & Q(**kwargs) if outputQ else Q(**kwargs)
        if outputQ: queryset = queryset.filter(outputQ)

        iTotalRecords = iTotalDisplayRecords = queryset.count() # Count how many records match the final criteria
        queryset = queryset[iDisplayStart:iDisplayEnd] # Get the slice

        # Create aaData array data
        aaData = []
        for aRow in queryset.values(*aFields):
            aRowKeys = aRow.keys()
            aRowValues = aRow.values()
            aRowList = []
            for iColIdx in range(0, iColumns):
                for idx, val in enumerate(aRowKeys):
                    if val == aFields[iColIdx]:
                        aRowList.append(str(aRowValues[idx]))
            if self.has_controls:
                aRowList.append(self.controls_column_markup)
            aaData.append(aRowList)

        # Render to json response
        return self.render_to_json({
            'aaData': aaData,
            'sEcho': sEcho,
            'iTotalRecords': iTotalRecords,
            'iTotalDisplayRecords': iTotalDisplayRecords,
            'sColumns': sColumns
        })

    def render_to_json(self, context, **response_kwargs):
        response = HttpResponse(json.dumps(context), mimetype='application/javascript')
        add_never_cache_headers(response)
        return response
    
    
class AdminSite(object):
    views = []
    
    def register(self, view):
        self.views.append([view, view.model._meta.verbose_name, unicode(view.model._meta.verbose_name_plural)])
        
    def get_urls(self):
        urlpatterns = patterns('', )
        for view, app_name, app_name_plural in self.views:
            urlpatterns += patterns('',
                url(r'^%s$'%app_name_plural, view.as_view(), name=app_name_plural),
                url(r'^%s/list$'%app_name_plural, view.as_view(), {'action': 'list'}, 'list_'+app_name_plural),
                url(r'^%s/add$'%app_name_plural, view.as_view(), {'action': 'add'}, 'add_'+app_name),
                url(r'^%s/edit/(?P<obj_id>[0-9]+)$'%app_name_plural, view.as_view(), {'action': 'edit'}, 'edit_'+app_name),
                url(r'^%s/view/(?P<obj_id>[0-9]+)$'%app_name_plural, view.as_view(), {'action': 'view'}, 'view_'+app_name),
                url(r'^%s/delete/(?P<obj_id>[0-9]+)$'%app_name_plural, view.as_view(), {'action': 'delete'}, 'delete_'+app_name),
            )
        return urlpatterns
    
    
site = AdminSite()