import floppyforms as forms

from ..library.models import Section


class SectionForm(forms.ModelForm):
    class Meta:
        model = Section