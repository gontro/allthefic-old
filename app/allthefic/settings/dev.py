from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ('localhost', '127.0.0.1')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'allthefic',
        'USER': 'allthefic',
        'PASSWORD': 'VEjJZBHXEec24W7yTf8TtvNFWBYEtfhb',
        'HOST': 'localhost',
        'PORT': '',
    },
    'OPTIONS': {
        'autocommit': True,
    }
}

MEDIA_ROOT = '/home/gontro/Projects/allthefic/web/media/'

STATIC_ROOT = '/home/gontro/Projects/allthefic/web/static/'

TEMPLATE_DIRS = (
    '/home/gontro/Projects/allthefic/web/templates'
)

SECRET_KEY = 'x!wc!u2yx9_hg4p!ur3xem)^z^d!vydy(z72bik=v4_orltm(('

MIDDLEWARE_CLASSES += ['debug_toolbar.middleware.DebugToolbarMiddleware']

INSTALLED_APPS += ['debug_toolbar']

INTERNAL_IPS = ('127.0.0.1',)
DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TOOLBAR_CALLBACK': False,
    'EXTRA_SIGNALS': [],
    'HIDE_DJANGO_SQL': False,
    'TAG': 'body',
    'ENABLE_STACKTRACES' : True,
}