var Atf = null, oDataTable = null;

Atf = {
    growl: function(data) {
        if (!data.status || !data.message) return false;

        var sIconClass = '';
        switch (data.status) {
            case 'success':
                sIconClass = 'icon-ok-sign';
                break;
            case 'info':
                sIconClass = 'icon-info-sign';
                break;
            case 'error':
                sIconClass = 'icon-warning-sign';
                break;
        }

        data.message = '<div class="pull-left"><i class="' + sIconClass + '" style="font-size:39px"></i></div><div style="margin-left:48px">' + data.message + '</div>';
        data.offset = data.offset ? data.offset : 0;

        $.bootstrapGrowl(data.message, {
            type: data.status,
            offset: {from: 'top', amount: 51 + data.offset}
        });

        return true;
    },

    getSelectedText: function() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.getSelection) {
            text = document.getSelection();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        return text;
    },

    ChapterRating: {
        init: function(sSubmitUrl, bIsAuthenticated) {
            var oBtnLikeChapter = $('.btn-like-chapter');
            var oBtnDislikeChapter = $('.btn-dislike-chapter');

            oBtnLikeChapter.on('click', function(){
                if (!bIsAuthenticated) {
                    $('#modal_login_required').modal('show');
                } else {
                    if (!oBtnLikeChapter.parent().hasClass('active')) {
                        var oIcon = $('i', oBtnLikeChapter);
                        oIcon.data('pclass', oIcon.attr('class')).removeClass().addClass('icon-spinner icon-spin');
                        $.getJSON(sSubmitUrl, null, function(data){
                            oIcon.removeClass().addClass(oIcon.data('pclass'));
                            if (data.status == 'success') {
                                oBtnLikeChapter.parent().addClass('active');
                                oBtnDislikeChapter.parent().removeClass('active');
                            }
                            Atf.growl(data);
                        });
                    }
                }
            });

            oBtnDislikeChapter.on('click', function(){
                if (!bIsAuthenticated) {
                    $('#modal_login_required').modal('show');
                } else {
                    if (!oBtnDislikeChapter.parent().hasClass('active')) {
                        var oIcon = $('i', oBtnDislikeChapter);
                        oIcon.data('pclass', oIcon.attr('class')).removeClass().addClass('icon-spinner icon-spin');
                        $.getJSON(sSubmitUrl, {dislike:true}, function(data){
                            oIcon.removeClass().addClass(oIcon.data('pclass'));
                            if (data.status == 'success') {
                                oBtnDislikeChapter.parent().addClass('active');
                                oBtnLikeChapter.parent().removeClass('active');
                            }
                            Atf.growl(data);
                        });
                    }
                }
            });
        }
    },

    Comments: {
        init: function() {
            var oBtnShowComments = $('.btn-show-comments');
            var oBtnHideComments = $('.btn-hide-comments');
            var oBodyWrapper = $('#body-wrapper');
            var oSidebarWrapper = $('#sidebar-wrapper');

            oBtnShowComments.on('click', function(){
                oBodyWrapper.removeClass().addClass('span10');
                oSidebarWrapper.show();
                oBtnShowComments.hide();
                oBtnHideComments.show();
            });

            oBtnHideComments.on('click', function(){
                oBodyWrapper.removeClass().addClass('span12');
                oSidebarWrapper.hide();
                oBtnShowComments.show();
                oBtnHideComments.hide();
            });
        }
    },

    Reviews: {
        init: function(sSubmitUrl) {
            $('.write-review textarea').redactor({
                convertDivs: true,
                convertLinks: false,
                minHeight: 200,
                buttons: ['bold', 'italic', 'underline'],
                allowedTags: ['p', 'b', 'i', 'u']
            });

            $('.write-review input, .write-review .redactor_editor, .write-review button').on('click', function(e){
                e.stopPropagation();
            });

            $('.write-review button').on('click', function(){
                var sUsername = $('.write-review input').val();
                var sReview = $('.write-review textarea').getCode();
                $.getJSON(sSubmitUrl, {username:sUsername, review:sReview}, function(data){
                    if (data.status == 'success') {
                        // @todo: Add message response
                    } else {
                        // @todo: Add exception handling
                    }
                });
            });
        }
    },

    TextControls: {
        init: function(oContent) {
            oContent = $(oContent);

            $('.btn-increase-font-size').on('click', function(){
                var iSize = parseInt(oContent.css('font-size')) + 2;
                if (iSize <= 48) {
                    oContent.css({'font-size': iSize + 'px', 'line-height': iSize + 2 + 'px'});
                }
            });

            $('.btn-decrease-font-size').on('click', function(){
                var iSize = parseInt(oContent.css('font-size')) - 2;
                if (iSize >= 10) {
                    oContent.css({'font-size': iSize + 'px', 'line-height': iSize + 2 + 'px'});
                }
            });
        }
    },

    GridLinks: {
        init: function() {
            $('.grid-item').on('click', function(){
                window.location.href = $(this).data('href');
            });
        }
    },

    Tracking: {
        initAuthor: function(sFollowUrl, sFavouriteUrl, bIsAuthenticated) {
            Atf.Tracking.query('.btn-follow-author', '.btn-unfollow-author', sFollowUrl, bIsAuthenticated);
            Atf.Tracking.query('.btn-favourite-author', '.btn-unfavourite-author', sFavouriteUrl, bIsAuthenticated);
        },

        initStory: function(sFollowUrl, sFavouriteUrl, bIsAuthenticated) {
            Atf.Tracking.query('.btn-follow-story', '.btn-unfollow-story', sFollowUrl, bIsAuthenticated);
            Atf.Tracking.query('.btn-favourite-story', '.btn-unfavourite-story', sFavouriteUrl, bIsAuthenticated);
        },

        initCommunity: function(sFollowUrl, sFavouriteUrl, bIsAuthenticated) {
            Atf.Tracking.query('.btn-follow-community', '.btn-unfollow-community', sFollowUrl, bIsAuthenticated);
            Atf.Tracking.query('.btn-favourite-community', '.btn-unfavourite-community', sFavouriteUrl, bIsAuthenticated);
        },

        query: function(oTrigger, oOpposite, sUrl, bIsAuthenticated) {
            oTrigger = $(oTrigger);
            oOpposite = $(oOpposite);

            oTrigger.on('click', function(){
                if (!bIsAuthenticated) {
                    $('#modal_login_required').modal('show');
                } else {
                    var pk = $(this).data('pk');
                    var oIcon = $('i', oTrigger);
                    oIcon.data('pclass', oIcon.attr('class')).removeClass().addClass('icon-spinner icon-spin');
                    $.getJSON(sUrl, null, function(data){
                        oIcon.removeClass().addClass(oIcon.data('pclass'));
                        if (data.status == 'success') {
                            oTrigger.hide();
                            oOpposite.show();
                        }
                        Atf.growl(data);
                    });
                }
            });

            oOpposite.on('click', function(){
                if (!bIsAuthenticated) {
                    $('#modal_login_required').modal('show');
                } else {
                    var pk = $(this).data('pk');
                    var oIcon = $('i', oOpposite);
                    oIcon.data('pclass', oIcon.attr('class')).removeClass().addClass('icon-spinner icon-spin');
                    $.getJSON(sUrl, {stop:true}, function(data){
                        oIcon.removeClass().addClass(oIcon.data('pclass'));
                        if (data.status == 'success') {
                            oOpposite.hide();
                            oTrigger.show();
                        }
                        Atf.growl(data);
                    });
                }
            });
        }
    },

    Tooltips: {
        init: function() {}
    },

    Popovers: {
        init: function() {
            $('.atf-popover').popover({placement:'right', html:true, trigger:'hover'});
        }
    },

    Nav: {
        chapter: function(oControls) {
            oControls = $(oControls);
            oControls.on('change', function(){
                window.location.href = $('option:selected', $(this)).val();
            });
        }
    },

    Forms: {
        preventEmptySubmit: function(oForm) {
            oForm = $(oForm);
            oForm.submit(function(){
                $(this).find('select').each(function(i,e){
                    if ($(e).val() == '') $(e).attr('disabled','disabled');
                });
                return true;
            });
        }
    },

    Messages: {
        flash: function(aMessages, sContainer) {
            for (var index in aMessages) {
                var container = $('#messages');
                if (sContainer) container = $(sContainer);
                container.append('<div class="alert alert-'+aMessages[index].code+'"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>'+aMessages[index].title+'</strong> '+aMessages[index].message+'</div>');
                if (!aMessages[index].sticky) $('div:visible', container).delay(3500).fadeOut();
            }
        }
    },

    Modals: {
        showSpinner: function(oModal) {
            oModal = $(oModal);
            var controls = $('.modal-footer', oModal);
            var spinner = $('img.spinner', controls);
            var buttons = $('button', controls);
            buttons.hide();
            spinner.show();
        },

        hideSpinner: function(oModal) {
            oModal = $(oModal);
            var controls = $('.modal-footer', oModal);
            var spinner = $('img.spinner', controls);
            var buttons = $('button', controls);
            spinner.hide();
            buttons.show();
        },

        ajaxDelete: function(oModal, oMsgContainer, sDeleteUrl) {
            if (confirm("Are you sure you want to delete this item?")) {
                Atf.Modals.showSpinner(oModal);
                $.getJSON(sDeleteUrl, null, function(data){
                    if (data.status == 'OK') {
                        oModal.modal('hide');
                        oDataTable.fnDraw();
                        Atf.Messages.flash(data.messages);
                    } else {
                        Atf.Messages.flash(data.messages, oMsgContainer);
                    }
                    Atf.Modals.hideSpinner(oModal);
                });
            }
        },

        ajaxSubmit: function(oModal, oMsgContainer) {
            var form = $('form', oModal);
            Atf.Modals.showSpinner(oModal);
            $.post(form.attr('action'), form.serialize(), function(data){
                if (data.status == 'OK') {
                    oModal.modal('hide');
                    oDataTable.fnDraw();
                    Atf.Messages.flash(data.messages);
                } else {
                    Atf.Messages.flash(data.messages, oMsgContainer);
                }
                Atf.Modals.hideSpinner(oModal);
            }, 'json');
        }
    },

    DataTables: {
        getDefaultConfig: function(aParams) {
            aParams['aAction'] = (typeof(aParams['aAction']) != 'undefined' ? aParams['aAction'] : 'edit');

            var oDataTablesDefault = {
                'bProcessing': true,
                'bServerSide': true,
                'bAutoWidth': false,
                'sAjaxSource': location.pathname+'/list'+location.search,
                'fnDrawCallback': Atf.DataTables.Events.apply,
                'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    return Atf.DataTables.rowCallback(aParams, nRow, aData, iDisplayIndex, iDisplayIndexFull);
                }
            };

            return $.extend(true, oDataTablesDefault, aParams);
        },

        rowCallback: function(aParams, nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var iItemId = aData[0];
            var sAction = '';

            if (aParams['aAction'] == 'view') sAction = '<button type="button" class="btn btn-mini" data-toggle="modal" data-target="#id_view_modal" data-remote="'+aParams['sPath']+'view/'+iItemId+'"><i class="icon-eye-open"></i> View</button>';
            else if (aParams['aAction'] == 'edit') sAction = '<button type="button" class="btn btn-mini" data-toggle="modal" data-target="#id_edit_modal" data-remote="'+aParams['sPath']+'edit/'+iItemId+'"><i class="icon-edit icon-white"></i> Edit</button>';

            $('td:last', nRow).html(sAction);
            return nRow;
        },

        Events: {
            apply: function() {}
        }
    }
};
