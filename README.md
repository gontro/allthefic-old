allthefic
=========

A fanfiction site created out of necessity.

Installation
------------

    sudo aptitude install build-essential python-dev postgresql postgresql-server-dev-all redis-server supervisor
    sudo pip install virtualenv
    virtualenv --no-site-packages env
    ./env/bin/pip install -r requirements.txt
    ln -s ../../../../app/allthefic env/lib/python2.7/site-packages/


Create app/allthefic/settings/local.py and add `from dev/staging/production import *`
Run web/perms.sh file to configure directory permissions.
Copy django-admin.py into env/bin and edit if needed.